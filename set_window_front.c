/*	set_window_front
	On EWMH-compliant window managers, sets the _NET_WM_STATE of the given
	window to _NET_WM_STATE_ABOVE to make it stay on top of all others besides
	_NET_WM_STATE_FULLSCREEN. If the window manager is not EWMH-compliant, 
	nothing is done.

	Arguments:
	Display *disp - a pointer to the X display
	Window wind - the window being moved to the front
*/
void set_window_front(Display *disp, Window wind)
{
	Atom wm_state, wm_state_above;
	XEvent event;

	if ((wm_state = XInternAtom(disp, "_NET_WM_STATE", False)) != None)
	{
		if ((wm_state_above = XInternAtom(disp, "_NET_WM_STATE_ABOVE", False))
			!= None)
		{
			/* sending a ClientMessage */
			event.xclient.type = ClientMessage;

			/* value unimportant in this case */
			event.xclient.serial = 0;

			/* coming from a SendEvent request, so True */
			event.xclient.send_event = True;

			/* the event originates from disp */
			event.xclient.display = disp;

			/* the window whose state will be modified */
			event.xclient.window = wind;

			/* the component Atom being modified in the window */
			event.xclient.message_type = wm_state;

			/* specifies that data.l will be used */
			event.xclient.format = 32;

			/* 1 is _NET_WM_STATE_ADD */
			event.xclient.data.l[0] = 1;

			/* the atom being added */
			event.xclient.data.l[1] = wm_state_above;

			/* unused */
			event.xclient.data.l[2] = 0;
			event.xclient.data.l[3] = 0;
			event.xclient.data.l[4] = 0;

			/* actually send the event */
			XSendEvent(disp, DefaultRootWindow(disp), False,
				SubstructureRedirectMask | SubstructureNotifyMask, &event);
		}
	}
}